<?php

    namespace App\Http\Controllers\Test;

    use App\Http\Controllers\Controller;
    use Barryvdh\Debugbar\Facades\Debugbar;
    use Illuminate\Http\Request;

    class TestController extends Controller
    {
        public function getName($name)
        {
            echo "TestController::class name: {$name}";
        }

        public function getPageById($pageId)
        {
            Debugbar::info('param', $pageId);

            return view('user', compact('pageId'));
//            return view('user', ['pageId' => $pageId]);
//            return view('user')->withPageId($pageId);
//            return view('user')->with(['pageId' => $pageId]);
        }
    }
