<?php

    use Illuminate\Support\Facades\Route;
    use App\Http\Controllers\Test\TestController;

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider and all of them will
    | be assigned to the "web" middleware group. Make something great!
    |
    */

//        dump([1, 2, 3, 4]);
//        dd([1, 2, 3, 4]); // with die();
//        dd(request());
    /*
        Route::get('/', function () {
            return view('welcome');
        });
    */// Analog =>

    Route::view('/', 'welcome');

    ///////////////////
    /*    Route::get('user/{id}', function ($id) {
            echo "user id: {$id}";
        });
    */// with check on numbers in id =>

    Route::get('user/{id}', function ($id) {
        echo "user id: {$id}";
    })->where('id', '[0-9]+');

    /////////////////////////
    Route::get('posts/{postId}/comments/{commentId}', function ($postId, $commentId) {
        echo "post id: {$postId}, comment id: {$commentId}.";
    });

    Route::get('folder/{folderId?}', function ($folderId = 0) {
        echo "Folder id: {$folderId}.";
    });

    Route::get('category_{name?}', function ($nameCategory = 'base') {
        echo "Category: {$nameCategory}.";
    });

    //////////////
//    Route::get('about', 'PageController@about'); // PageController => class, @about => method

//    Route::get('test/{name}', 'App\Http\Controllers\Test\TestController@getName');
//    or
//    Route::get('test/{name}', [TestController::class ,'getName']);
//    Route::get('test/{pageId}', [TestController::class ,'getPage']);
    Route::get('page/{pageId}', [TestController::class ,'getPageById']);

    //////////////////
    Route::get('route', function () {
        echo 'First route.';
    });

    Route::get('route', function () {
        echo 'Second route.';
    });

